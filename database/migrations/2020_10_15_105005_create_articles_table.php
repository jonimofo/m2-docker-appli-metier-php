<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('code');
            $table->string('name');
            $table->string(
                'univers');

            // TVA
            $table->integer('sales_tva');
            $table->integer('purchase_tva');

            // UDF
            $table->boolean('splittable');
            $table->boolean('variable_weight');
            $table->string('nature');
            $table->boolean('alimentaire');

/*            $table->string('dlc_ddm');
            $table->integer('dl_totale');
            $table->integer('dl_min_achat');
            $table->integer('dl_min_vente');

            // Dimensions
            $table->integer('width');
            $table->integer('lenght');
            $table->integer('height');

            // Poids
            $table->integer('colis_weigth');

            // Code barres
            $table->string('ean13');
            $table->string('ean14');

            // Différents types d'unités
            $table->foreignId('base_unit_id')->constrained('units');
            $table->foreignId('purchase_unit_id')->constrained('units');
            $table->foreignId('sales_unit_id')->constrained('units');

            // Groupe article
            $table->foreignId('article_group_id')->constrained();*/

            // Fournisseur
            $table->foreignId('supplier_id')
                ->nullable()
                ->constrained();

            $table->string('article_code_supplier')
                ->nullable();
//            $table->string('site_enlevement');

            /*
            computed :
            - volume
            - equivalent palette
            -
            */
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
