<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnitsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('units')->delete();

        DB::table('units')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'BAC',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'PCE',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'SHT',
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'COL',
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'SAU',
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'FBT',
            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'CCH',
            ),
            7 =>
            array (
                'id' => 8,
                'name' => 'PAL',
            ),
            8 =>
            array (
                'id' => 9,
                'name' => 'POT',
            ),
            9 =>
            array (
                'id' => 10,
                'name' => 'KG',
            ),
            10 =>
            array (
                'id' => 11,
                'name' => 'LIT',
            ),
            11 =>
            array (
                'id' => 12,
                'name' => 'PUB',
            ),
            12 =>
            array (
                'id' => 13,
                'name' => 'MEU',
            ),
            13 =>
            array (
                'id' => 14,
                'name' => 'BQT',
            ),
            14 =>
            array (
                'id' => 15,
                'name' => 'BRI',
            ),
            15 =>
            array (
                'id' => 16,
                'name' => 'PCK',
            ),
            16 =>
            array (
                'id' => 17,
                'name' => 'SAU',
            ),
            17 =>
            array (
                'id' => 18,
                'name' => 'BID',
            ),
            18 =>
            array (
                'id' => 19,
                'name' => 'BTE',
            ),
            19 =>
            array (
                'id' => 20,
                'name' => 'OUT',
            ),
            20 =>
            array (
                'id' => 21,
                'name' => 'SAC',
            ),
        ));


    }
}
