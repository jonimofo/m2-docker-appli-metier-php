<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticleGroupsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('article_groups')->delete();

        DB::table('article_groups')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'ACO-ALCOOLS',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'ACO-AROMES',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'ACO-COLORANTS',
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'ACO-EPICE CONDIMENT',
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'ACO-FINANCIER',
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'ACO-HERB AROMATIQUE',
            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'ACO-SAUCE SUCRE TOP',
            ),
            7 =>
            array (
                'id' => 8,
                'name' => 'ACO-SAUCES ET FONDS',
            ),
            8 =>
            array (
                'id' => 9,
                'name' => 'AMG-FINANCIER',
            ),
            9 =>
            array (
                'id' => 10,
                'name' => 'AMG-HUILE',
            ),
            10 =>
            array (
                'id' => 11,
                'name' => 'AMG-MARGARINE',
            ),
            11 =>
            array (
                'id' => 12,
                'name' => 'BOF-BEURRE',
            ),
            12 =>
            array (
                'id' => 13,
                'name' => 'BOF-CREME',
            ),
            13 =>
            array (
                'id' => 14,
                'name' => 'BOF-FINANCIER',
            ),
            14 =>
            array (
                'id' => 15,
                'name' => 'BOF-FROMAGE',
            ),
            15 =>
            array (
                'id' => 16,
                'name' => 'BOF-LAIT',
            ),
            16 =>
            array (
                'id' => 17,
                'name' => 'BOF-OVOPRODUITS',
            ),
            17 =>
            array (
                'id' => 18,
                'name' => 'BOF-VACHE NOIRE',
            ),
            18 =>
            array (
                'id' => 19,
                'name' => 'BOF-YAOURT',
            ),
            19 =>
            array (
                'id' => 20,
                'name' => 'BOI-CAFE',
            ),
            20 =>
            array (
                'id' => 21,
                'name' => 'BOI-FINANCIER',
            ),
            21 =>
            array (
                'id' => 22,
                'name' => 'BOI-JUS',
            ),
            22 =>
            array (
                'id' => 23,
                'name' => 'BOI-SIROPS',
            ),
            23 =>
            array (
                'id' => 24,
                'name' => 'BOI-SOUPE',
            ),
            24 =>
            array (
                'id' => 25,
                'name' => 'BOI-THE',
            ),
            25 =>
            array (
                'id' => 26,
                'name' => 'BPA-FESTIF',
            ),
            26 =>
            array (
                'id' => 27,
                'name' => 'BPA-FINANCIER',
            ),
            27 =>
            array (
                'id' => 28,
                'name' => 'BPA-PAINS',
            ),
            28 =>
            array (
                'id' => 29,
                'name' => 'BPA-PATISSERIE',
            ),
            29 =>
            array (
                'id' => 30,
                'name' => 'BPA-PDTS SERVICES',
            ),
            30 =>
            array (
                'id' => 31,
                'name' => 'BPA-RÉCEP SUCRÉE',
            ),
            31 =>
            array (
                'id' => 32,
                'name' => 'BPA-RECEPTION SALÉE',
            ),
            32 =>
            array (
                'id' => 33,
                'name' => 'BPA-SNACKING',
            ),
            33 =>
            array (
                'id' => 34,
                'name' => 'BPA-VIEN AMERICAINE',
            ),
            34 =>
            array (
                'id' => 35,
                'name' => 'BPA-VIENNOISERIE',
            ),
            35 =>
            array (
                'id' => 36,
                'name' => 'CHA-DEC-EMINCE-HACHE',
            ),
            36 =>
            array (
                'id' => 37,
                'name' => 'CHA-FINANCIER',
            ),
            37 =>
            array (
                'id' => 38,
                'name' => 'CHA-PCE ANATOMIQUE',
            ),
            38 =>
            array (
                'id' => 39,
                'name' => 'CHA-PLATS CUISINES',
            ),
            39 =>
            array (
                'id' => 40,
                'name' => 'CHA-SALAISON',
            ),
            40 =>
            array (
                'id' => 41,
                'name' => 'CNF-AU CHOCOLAT',
            ),
            41 =>
            array (
                'id' => 42,
                'name' => 'CNF-AU FRUIT',
            ),
            42 =>
            array (
                'id' => 43,
                'name' => 'CNF-FINANCIER',
            ),
            43 =>
            array (
                'id' => 44,
                'name' => 'CNF-GUIMAUVE',
            ),
            44 =>
            array (
                'id' => 45,
                'name' => 'EMB-DESTINE BOUTIQUE',
            ),
            45 =>
            array (
                'id' => 46,
                'name' => 'EMB-DESTINE LABO',
            ),
            46 =>
            array (
                'id' => 47,
                'name' => 'EMB-FEVES',
            ),
            47 =>
            array (
                'id' => 48,
                'name' => 'EMB-FINANCIER',
            ),
            48 =>
            array (
                'id' => 49,
                'name' => 'EMB-TECH ET NET',
            ),
            49 =>
            array (
                'id' => 50,
                'name' => 'FEC-FINANCIER',
            ),
            50 =>
            array (
                'id' => 51,
                'name' => 'FEC-PATE ALIMENT',
            ),
            51 =>
            array (
                'id' => 52,
                'name' => 'FEC-POMME DE TERRE',
            ),
            52 =>
            array (
                'id' => 53,
                'name' => 'FEC-RIZ',
            ),
            53 =>
            array (
                'id' => 54,
                'name' => 'FRU-BILLE/ENTIER',
            ),
            54 =>
            array (
                'id' => 55,
                'name' => 'FRU-COULIS-FOURRAGE',
            ),
            55 =>
            array (
                'id' => 56,
                'name' => 'FRU-FINANCIER',
            ),
            56 =>
            array (
                'id' => 57,
                'name' => 'FRU-FRUITS SECS',
            ),
            57 =>
            array (
                'id' => 58,
                'name' => 'FRU-MELANGES',
            ),
            58 =>
            array (
                'id' => 59,
                'name' => 'FRU-PUREE DECOUPE',
            ),
            59 =>
            array (
                'id' => 60,
                'name' => 'GLA-CREME GLACEE',
            ),
            60 =>
            array (
                'id' => 61,
                'name' => 'GLA-DESSERT GLACE',
            ),
            61 =>
            array (
                'id' => 62,
                'name' => 'GLA-FINANCIER',
            ),
            62 =>
            array (
                'id' => 63,
                'name' => 'GLA-GLACE HYDRIQUE',
            ),
            63 =>
            array (
                'id' => 64,
                'name' => 'GLA-SORBET',
            ),
            64 =>
            array (
                'id' => 65,
                'name' => 'LAB-CHOCOLATS',
            ),
            65 =>
            array (
                'id' => 66,
                'name' => 'LAB-DECORS PPE',
            ),
            66 =>
            array (
                'id' => 67,
                'name' => 'LAB-FINANCIER',
            ),
            67 =>
            array (
                'id' => 68,
                'name' => 'LAB-GLAC NAPP FOURR',
            ),
            68 =>
            array (
                'id' => 69,
                'name' => 'LAB-LEV AMELIO GELIF',
            ),
            69 =>
            array (
                'id' => 70,
                'name' => 'LAB-POUDRE ET MIX',
            ),
            70 =>
            array (
                'id' => 71,
                'name' => 'LAB-SEL & SUCRE',
            ),
            71 =>
            array (
                'id' => 72,
                'name' => 'LEG-BILLES/ENTIERS',
            ),
            72 =>
            array (
                'id' => 73,
                'name' => 'LEG-CUISINES/GARNIT',
            ),
            73 =>
            array (
                'id' => 74,
                'name' => 'LEG-FINANCIER',
            ),
            74 =>
            array (
                'id' => 75,
                'name' => 'LEG-MELANGES',
            ),
            75 =>
            array (
                'id' => 76,
                'name' => 'LEG-PUREES DECOUPE',
            ),
            76 =>
            array (
                'id' => 77,
                'name' => 'MAT-FINANCIER',
            ),
            77 =>
            array (
                'id' => 78,
                'name' => 'MAT-MATERIEL CHAUD',
            ),
            78 =>
            array (
                'id' => 79,
                'name' => 'MAT-USTENSILES',
            ),
            79 =>
            array (
                'id' => 80,
                'name' => 'MER-CRUSTACES',
            ),
            80 =>
            array (
                'id' => 81,
                'name' => 'MER-FINANCIER',
            ),
            81 =>
            array (
                'id' => 82,
                'name' => 'MER-PLATS CUISINES',
            ),
            82 =>
            array (
                'id' => 83,
                'name' => 'MER-POISSON',
            ),
            83 =>
            array (
                'id' => 84,
                'name' => 'MER-SAURISSERIE',
            ),
            84 =>
            array (
                'id' => 85,
                'name' => 'MKT-AID VENT GRAT',
            ),
            85 =>
            array (
                'id' => 86,
                'name' => 'MKT-AID VENT REFACT',
            ),
            86 =>
            array (
                'id' => 87,
                'name' => 'MKT-FINANCIER',
            ),
            87 =>
            array (
                'id' => 88,
                'name' => 'VIA-DEC-EMINCE-HACHE',
            ),
            88 =>
            array (
                'id' => 89,
                'name' => 'VIA-FINANCIER',
            ),
            89 =>
            array (
                'id' => 90,
                'name' => 'VIA-PCE ANATOMIQUE',
            ),
            90 =>
            array (
                'id' => 91,
                'name' => 'VIA-PLATS CUISINES',
            ),
            91 =>
            array (
                'id' => 92,
                'name' => 'VOL-DEC-EMINCE-HACHE',
            ),
            92 =>
            array (
                'id' => 93,
                'name' => 'VOL-FINANCIER',
            ),
            93 =>
            array (
                'id' => 94,
                'name' => 'VOL-PCE ANATOMIQUE',
            ),
            94 =>
            array (
                'id' => 95,
                'name' => 'VOL-PLATS CUISINES',
            ),
        ));


    }
}
