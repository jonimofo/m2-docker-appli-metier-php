<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\FournisseurController;
use App\Http\Controllers\MercurialeController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('pages/home');
});

Route::get('/mercuriale', [MercurialeController::class,'index'])->name('mercuriale');
Route::get('/clients', [ClientController::class,'index'])->name('clients');
Route::get('/fournisseurs', [FournisseurController::class,'index'])->name('fournisseurs');

Route::prefix('/articles')->group(function () {
    Route::get('/', [ArticleController::class,'index'])->name('articles');
    Route::get('/create', [ArticleController::class, 'create'])->name('articles.create');
    Route::post('/', [ArticleController::class, 'store'])->name('articles.store');
});
