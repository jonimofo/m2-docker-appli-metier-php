# CI/CD d'une application PHP avec Gitlab CI
## Contenu du dépôt
Une application PHP qui servira d'outil pour les salariés de notre entreprise. Cette appliation est packagée sous la forme d'un ```docker-compose``` qui déploie l'environnement de dev avec les services suivants :
- php 7
- mysql 5.7
- nginx
- composer
- artisan

## Mise en place du projet 

```
git clone https://gitlab.com/qdvseb/PortailSEB.git
docker-compose up --build -d
cp .env.example .env
docker-compose run composer install
docker-compose run artisan key:generate
docker-compose run artisan migrate:refresh
docker-compose run artisan db:seed
```

Cette suite d'opération va :
- Récupérer le projet sur votre machine
- Construire l'image Docker du projet et lancer le docker-compose pour lancer tous les services
dont le projet a besoin pour fonctionner
- Créer le fichier des variables d'environnement à partir du fichier d'exemple
- Installer les dépendances du projet avec composer
- Générer une clef de chiffrement
- Executer les migrations de la base de données
- Garnir la base de données avec les seeds 

## Lancement des tests

Les tests s'écrivent dans le dossier '''tests''' à la racine, vous pouvez créer un fichier de test avec la commande : 
```
docker-compose run artisan make:test VotreFichierDeTest
```

Puis lancer l'éxecution des tests avec :
```
docker-compose run artisan test
```

## CI/CD avec Gitlab CI
Lorsqu'un push est réalisé, une suite d'opération va être réalisée : 
### Stage test
Dans la phase de test, le projet va être construit dans le runner et 2 opérations seront lancées : 

- Lancement du linter avec :
```
vendor/bin/phpcs
```
- Lancement des tests avec :
```
php artisan test
```

### Stage build
Dans la phase de build, l'image du projet va être construite et envoyée dans le registry de gitlab.

### Stage push
Dans la phase de push, l'objectif est de tagger la branche master en tant que '''latest'''.

## Difficultés rencontrées

- Connexion à la BDD dans le runner
- Compréhension dans l'écriture du fichier gitlab-ci.yml
- Compréhension de l'intérêt de push une image dans le registry

## Ressources
- [edbizzaro gitlab pipelines examples](https://github.com/edbizarro/gitlab-ci-pipeline-php)
- [Setting up GitLab CI/CD](https://docs.gitlab.com/ee/ci/examples/laravel_with_gitlab_and_envoy/#installing-dependencies-with-composer)
- [Building Docker images with GitLab CI/CD](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html)
- [Gitlab CI : example PHP & NPM](https://docs.gitlab.com/ee/ci/examples/deployment/composer-npm-deploy.html)
- [Best practices for building docker images with GitLab CI](https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/)

---

# L'application PHP : SEB Tools

## Fiche de création article

Le but, à terme, est de permettre aux utilisateurs de renseigner tous les champs nécessaires à la création d'un article et lorsque que tous les champs ont été complété, 
l'article doit se créer directement dans SAP via la DI API.
Pour l'instant, le but est de seulement préparer le formulaire avec tous les champs nécessaires pour guider l'utilisateur dans la création d'un article

La page se présentera comme une liste d'article, et en l'occurence ceux qui ont déjà été complété sur la plateforme avec leur statut 'Créé', 'En cours'.




## Mercuriale Helper

Le but du projet est de fournir une aide à la lecture de la mercuriale (pour Nathalie).
Lorsque Nathalie verra une modification de la mercuriale sur l'intranet, elle téléchargera la mercuriale et 
devra uploader sur l'application avec un Drag & Drop.  

La mercuriale sera ensuite parcourue pour créer / mettre à jour les données des articles dans une base de données.

Au 1er import de la mercuriale, il faudra se contenter d'enregistrer les données de l'article dans sa table.  
##### L'application devra gérer : 
- Les caractères spéciaux dans certains champs
- La bonne récupération des articles quelle que soit la forme de la mercuriale (savoir où sont les champs de titre,
savoir où commence et où se finit la liste des articles)
- Une vérification de la nature des colonnes doit être faite : c'est-à-dire qu'il ne faut pas partir du principe que
"colonne 26 = Libellé". Il faut vérifier le titre et assigner "dynamiquement" la nature de la colonne. 
(Si on voit le titre "Libellé Fournisseur", on associe cette colonne à cette donnée)
- Il faut faire la liste de tous les champs que l'on attend, et s'inquiéter s'il y en a un qu'on trouve pas
- Il faut gérer le cas où le titre du champ aurait "un peu" changé, et prévoir un test qui permet de tester la ressemblance entre ce que l'on attend 
et ce que l'on a. Pour quand meme détecter le champ meme si 1 ou 2 lettres ont changés.
- Lorsqu'un second import est réalisé, il faut mettre à jour les champs qui ont changé pour chaque article et surtout enregistrer la précédente 
valeur dans une table "log_history" avec le code article, le nom du champ qui a été mise à jour et sa valeur. 
Cela va permettre de conserver simplement toutes les modifications réalisées.
- À la fin d'import, on doit signaler à l'utilisateur tous les champs qui ont été mise à jour pour chaque article 
(car la personne qui modifie la mercuriale doit signaler ses mise à jour mais peut oublier de le faire)
(ainsi que les nouveaux articles et ceux qui ont disparu depuis le dernier import)
- Il faut pouvoir afficher la liste des articles (à voir sous quel forme (handsontable))
- Au survol d'une valeur, pouvoir afficher l'historique des changements


##### Établir la liste des choses bloquantes qui feraient que le parsage serait impossible : 
- L'absence des cellules de titres (comment les détecter ?) 
- Doublons dans les titres de colonnes -> il y en aura car il y a les zones P1/P2/P3
- Des champs absents (que l'on n’arrive pas à détecter car le titre a trop changé)




Pour accepter l'upload de fichier >2MB 

edit php.ini (cli)

upload_max_filesize = 512M
