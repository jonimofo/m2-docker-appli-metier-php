<?php

namespace Tests\Feature;

use App\Http\Livewire\ArticleForm;
use App\Models\Article;
use Livewire\Livewire;
use Tests\TestCase;

class ArticleFormTest extends TestCase
{

    /** @test that we can create an article */
    public function canCreateArticle()
    {
        Livewire::test(ArticleForm::class)
            ->set('code', 27456)
            ->set('name', 'TARTE AU CITRON')
            ->set('univers', 'SEB')
            ->set('supplierId', 1)
            ->set('nature', 'SURGELE')
            ->call('submit');

        $this->assertTrue(Article::whereCode(27456)->exists());
    }

    /** @test */
    public function fieldsAreRequired()
    {
        Livewire::test(ArticleForm::class)
            ->set('code')
            ->set('name', '')
            ->set('univers', '')
            ->set('supplierId')
            ->set('nature', '')
            ->call('submit')
            ->assertHasErrors([
                'code' => 'required',
                'name' => 'required',
                'univers' => 'required',
                'supplierId' => 'required',
                'nature' => 'required',
            ]);
    }

    /** @test */
    public function isRedirectedToArticlesPageAfterCreation()
    {
        Livewire::test(ArticleForm::class)
            ->set('code', 27456)
            ->set('name', 'TARTE AU CITRON')
            ->set('univers', 'SEB')
            ->set('supplierId', 1)
            ->set('nature', 'SURGELE')
            ->call('submit')
            ->assertRedirect('/articles');
    }
}
