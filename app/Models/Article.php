<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class Article
 *
 * @package App\Models
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $code
 * @property string $name
 * @property string $univers
 * @property int $sales_tva
 * @property int $purchase_tva
 * @property int $splittable
 * @property int $variable_weight
 * @property string $nature
 * @property int $alimentaire
 * @property int|null $supplier_id
 * @property string|null $article_code_supplier
 * @property-read ArticleGroup $articleGroup
 * @property-read Unit $baseUnit
 * @property-read Unit $purchaseUnit
 * @property-read Unit $salesUnit
 * @property-read Supplier|null $supplier
 * @method static Builder|Article newModelQuery()
 * @method static Builder|Article newQuery()
 * @method static Builder|Article query()
 * @method static Builder|Article whereAlimentaire($value)
 * @method static Builder|Article whereArticleCodeSupplier($value)
 * @method static Builder|Article whereCode($value)
 * @method static Builder|Article whereCreatedAt($value)
 * @method static Builder|Article whereId($value)
 * @method static Builder|Article whereName($value)
 * @method static Builder|Article whereNature($value)
 * @method static Builder|Article wherePurchaseTva($value)
 * @method static Builder|Article whereSalesTva($value)
 * @method static Builder|Article whereSplittable($value)
 * @method static Builder|Article whereSupplierId($value)
 * @method static Builder|Article whereUnivers($value)
 * @method static Builder|Article whereUpdatedAt($value)
 * @method static Builder|Article whereVariableWeight($value)
 * @mixin Eloquent
 */
class Article extends Model
{
    use HasFactory;

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function baseUnit()
    {
        return $this->belongsTo(Unit::class, 'base_unit_id');
    }

    public function purchaseUnit()
    {
        return $this->belongsTo(Unit::class, 'purchase_unit_id');
    }

    public function salesUnit()
    {
        return $this->belongsTo(Unit::class, 'sales_unit_id');
    }

    public function articleGroup()
    {
        return $this->belongsTo(ArticleGroup::class, 'article_group_id');
    }
}
