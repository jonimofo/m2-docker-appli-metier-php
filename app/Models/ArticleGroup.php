<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ArticleGroup
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ArticleGroup whereName($value)
 * @mixin \Eloquent
 */
class ArticleGroup extends Model
{
    use HasFactory;
}
