<?php

namespace App\Http\Livewire;

use App\Models\Article;
use App\Models\Supplier;
use Livewire\Component;

class ArticleForm extends Component
{

    public $code;
    public $name;
    public $univers;
    public $supplierId;
    public $articleCodeSupplier;
    public $salesTva = "5,5";
    public $purchaseTva = "5,5";
    public $splittable = false;
    public $variableWeight = false;
    public $alimentaire = true;
    public $nature;

    protected $rules = [
        'code' => 'required|integer',
        'name' => 'required',
        'univers' => 'required',
        'supplierId' => 'required',
        'salesTva' => 'required',
        'purchaseTva' => 'required',
        'nature' => 'required',
    ];

    public function submit()
    {
        $this->validate();
        $article = new Article;
        $article->code = $this->code;
        $article->name = $this->name;
        $article->univers = $this->univers;
        $article->supplier_id = $this->supplierId;
        $article->article_code_supplier = $this->articleCodeSupplier;
        $article->sales_tva = (int)$this->salesTva;
        $article->purchase_tva = (int)$this->purchaseTva;
        $article->splittable = $this->splittable;
        $article->variable_weight = $this->variableWeight;
        $article->alimentaire = $this->alimentaire;
        $article->nature = $this->nature;

        $article->save();

        return redirect()->to('articles');
    }

    public function render()
    {
        return view('livewire.article-form', [
            'suppliers' => Supplier::all(),
        ]);
    }
}
