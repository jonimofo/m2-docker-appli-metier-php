<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class UploadMercuriale extends Component
{

    use WithFileUploads;

    public $mercuriale;

    public function updatedMercuriale()
    {
        $name = Carbon::now()->format("Ymd-His") . '_' . $this->mercuriale->getClientOriginalName();
        $this->mercuriale->storeAs('mercuriales', $name);
        Storage::delete(Storage::files('livewire-tmp'));
    }

    public function render()
    {
        return view('livewire.upload-mercuriale');
    }
}
