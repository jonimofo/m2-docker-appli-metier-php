<?php

namespace App\Http\Controllers;

use App\Models\Supplier;

class FournisseurController extends Controller
{
    public function index()
    {
        $suppliers = Supplier::all();
        return view('pages/fournisseurs', ['suppliers' => $suppliers]);
    }
}
