<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index()
    {
        return view('pages/articles/index');
    }

    public function create()
    {
        return view('pages/articles/create');
    }

    public function store(Request $request)
    {
    }
}
