@extends('layouts.app')

@section('content')

    <div class="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
        @livewire('upload-mercuriale')
    </div>

@endsection
