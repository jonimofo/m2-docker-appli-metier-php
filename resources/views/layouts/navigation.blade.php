<nav class="bg-gray-800">
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex items-center justify-between h-16">
            <div class="flex items-center">
                <div class="flex-shrink-0">
                    <a href="/"><img class="w-24" src="{{ asset('/assets/images/logo-seb-distribution.jpg') }}" alt="SEB logo"/></a>
                </div>
                <div class="hidden md:block">
                    <div class="ml-10 flex items-baseline">
                        <a href="{{ route('articles') }}" class="{{ Route::is('articles') ? 'text-white bg-gray-700' : '' }} ml-4 px-3 py-2 rounded-md text-sm font-medium text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700">Articles</a>
                        <a href="{{ route('clients') }}" class="{{ Route::is('clients') ? 'text-white bg-gray-700' : '' }} ml-4 px-3 py-2 rounded-md text-sm font-medium text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700">Clients</a>
                        <a href="{{ route('fournisseurs') }}" class="{{ Route::is('fournisseurs') ? 'text-white bg-gray-700' : '' }} ml-4 px-3 py-2 rounded-md text-sm font-medium text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700">Fournisseurs</a>
                        <a href="{{ route('mercuriale') }}" class="{{ Route::is('mercuriale') ? 'text-white bg-gray-700' : '' }} ml-4 px-3 py-2 rounded-md text-sm font-medium text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700">Mercuriale</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>


