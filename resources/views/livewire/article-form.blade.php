<div class="flex ml-4 mt-4 border p-4 w-1/2">
    <form wire:submit.prevent="submit">
        @csrf

        <label>
            <input type="text" wire:model="code" placeholder="Code Article"
                   class="form-input w-3/12 font-semibold @error('code') border-red-600 @enderror">
        </label>

        <label>
            <select wire:model="univers" class="form-select font-semibold @error('univers') border-red-600 @enderror">
                <option value="" selected>Univers</option>
                <option>CDP</option>
                <option>EURO DISTRIBUTION</option>
                <option>SEB</option>
                <option>LA CARTE</option>
                <option>CONCEPT AFS</option>
                <option>UNIVERS NON DEFINI</option>
            </select>
        </label>

        <section class="block mt-4">
            <label class="font-semibold">
                Fractionnable :
                <input type="checkbox" wire:model="splittable" class="form-checkbox w-5 h-5">
            </label>

            <label class="font-semibold ml-5">
                Poids variable :
                <input type="checkbox" wire:model="variableWeight" class="form-checkbox w-5 h-5">
            </label>
        </section>

        <label>
            <input type="text" wire:model="name" placeholder="Libellé Article"
                   class="form-input mt-1 block font-semibold @error('name') border-red-600 @enderror">
        </label>

        <label>
            <select wire:model="supplierId"
                    class="form-select font-semibold mt-1 w-6/12 @error('supplierId') border-red-600 @enderror">
                <option value="" class="text-gray-700" selected>Fournisseur</option>
                @foreach($suppliers as $supplier)
                    <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                @endforeach
            </select>
        </label>

        <label>
            <input type="text" wire:model="articleCodeSupplier" placeholder="Code Article Fournisseur"
                   class="form-input mt-1 font-semibold w-3/12">
        </label>

        <section class="block">
            <label class="font-semibold">TVA à la vente : </label>
            <select wire:model="salesTva" class="form-select font-semibold mt-1">
                <option value="5,5">5,5%</option>
                <option value="20">20%</option>
            </select>

            <label class="font-semibold">TVA à l'achat</label>
            <select wire:model="purchaseTva" class="form-select font-semibold mt-1">
                <option value="5,5">5,5%</option>
                <option value="20">20%</option>
            </select>
        </section>

        <section class="block mt-4">
            <label class="font-semibold">
                Alimentaire :
                <input type="checkbox" wire:model="alimentaire" class="form-checkbox w-5 h-5">
            </label>

            <select wire:model="nature" class="form-select font-semibold ml-5 @error('nature') border-red-600 @enderror">
                <option value="" selected>Nature</option>
                <option>SURGELE</option>
                <option>FRAIS</option>
                <option>SEC</option>
            </select>
        </section>

        <button type="submit" class="block mt-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
            Sauvegarder Article
        </button>

    </form>
</div>
